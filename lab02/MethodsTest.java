public class MethodsTest {
    public static void main(String[] args) {
        SecondClass sc = new SecondClass();

        int x = 10;
        System.out.println(x);
        methodNoInputNoReturn();
        System.out.println(x);

        methodOneInputNoReturn(10);
        methodOneInputNoReturn(x);
        methodOneInputNoReturn(x + 50);

        methodOneInputNoReturn(x + 10, x);

        int z = methodNoInputReturnInt();
        System.out.println("output from the methodNoInputReturnInt: " + z);

        System.out.println("output from the sumSquareRoot: " + sumSquareRoot(6, 3));

        String s1 = "hello";
        String s2 = "goodbye";

        System.out.println("length of hello: " + s1.length());
        System.out.println("length of goodbye: " + s2.length());

        System.out.println("output from the addOne: " + SecondClass.addOne(x));
        System.out.println("output from the addTwo: " + sc.addTwo(x));

    }

    public static void methodNoInputNoReturn() {
        System.out.println("I'm in a method that takes no input and returns nothing");
        int x = 50;
        System.out.println(x);

    }

    public static void methodOneInputNoReturn(int youCanCallThisWhateverYouLike) {
        System.out.println("Inside the method one input no return");
        System.out.println(youCanCallThisWhateverYouLike);
    }

    public static void methodOneInputNoReturn(int x, double y) {
        System.out.println("Inside the method two input no return");
        System.out.println(x);
        System.out.println(y);
    }

    public static int methodNoInputReturnInt() {
        return 6;
    }

    public static double sumSquareRoot(int x, int y) {
        double result = x + y;
        return Math.sqrt(result);
    }
}