import java.util.Scanner;

public class PartThree {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		AreaComputations calculations = new AreaComputations();

		// input
		System.out.println("give side for square");
		int sideSquare = scan.nextInt();
		System.out.println("give side for length of rectangle");
		int lengthRectangle = scan.nextInt();
		System.out.println("give side for width of rectangle");
		int widthRectangle = scan.nextInt();

		// calculs
		int areaOfSquare = AreaComputations.areaSquare(sideSquare);
		int areaOfRectangle = calculations.areaRectangle(lengthRectangle, widthRectangle);

		// output
		System.out.println("this is area of square: " + areaOfSquare);
		System.out.println("this is area of rectangle: " + areaOfRectangle);

		scan.close();
	}
}
